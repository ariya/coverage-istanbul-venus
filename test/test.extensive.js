/**
 * @venus-library jasmine
 * @venus-code ../sqrt.js
 */

describe("sqrt", function() {
    it('computes the square root of 9 as 3', function() {
        expect(My.sqrt(9)).toBeCloseTo(3, 10);
    });

    it('computes the square root of 16 as 4', function() {
        expect(My.sqrt(16)).toBeCloseTo(4, 10);
    });

    it('computes the square root of 25 as 5', function() {
        expect(My.sqrt(25)).toBeCloseTo(5, 10);
    });

    it('computes the square root of 36 as 6', function() {
        expect(My.sqrt(36)).toBeCloseTo(6, 10);
    });

    it('computes the square root of 49 as 7', function() {
        expect(My.sqrt(49)).toBeCloseTo(7, 10);
    });
});
