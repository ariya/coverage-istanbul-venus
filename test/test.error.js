/**
 * @venus-library qunit
 * @venus-code ../sqrt.js
 */

test("sqrt", function() {

    throws(
        function () {
            My.sqrt(-1000);
        },
        "sqrt can't work on negative number",
        "throws an exception when given -1000"
    );


    throws(
        function () {
            My.sqrt(1 * -8);
        },
        "sqrt can't work on negative number",
        "throws an exception when given a negative"
    );
});
