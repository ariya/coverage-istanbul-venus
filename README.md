This repo contains an example setup for tracking code coverage of
JavaScript unit tests via [Venus.js](http://www.venusjs.org) and
[Istanbul](http://gotwarlost.github.io/istanbul/).

Run the following:

```
npm install
npm test
```

and then open the code coverage report under the subdirectory `coverage`.

The unit tests are written using different test libraries:
[Mocha](http://visionmedia.github.io/mocha),
[Jasmine](http://pivotal.github.io/jasmine/),
[QUnit](http://qunitjs.com/). This is possible because [Venus.js](http://www.venusjs.org) supports such a
mix-and-match.

The tests are executed using [PhantomJS](http://phantomjs.org). It is easy to [configure](https://venusjs.readthedocs.org/en/latest/tutorials/environments.html) Venus.js to run the tests on Selenium Grid or Sauce Labs.

